<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>actividad9</title>
</head>
<body>
   <?php
   
// Ejercicio 1

$x = 0;

while($x < 5){
    echo "*"."<br>";
    $x++;
}

echo"<br>";

// Ejercicio 2

for ($i=0; $i < 5 ; $i++) { 
    for ($u=0; $u < 4; $u++) { 
        echo "*";
    }
    echo"<br>";
}

echo"<br>";
echo"<br>";

// Ejercicio 3

$piloto = "";

for ($t=0; $t < 6; $t++) { 
    $piloto.="*";
    echo $piloto;
    echo"<br>";
}

echo"<br>";
echo"<br>";

// Ejercicio 4

$piloto2 = "*";
$piloto3 = "*";
$piloto4 = "**";

for ($s=0; $s < 1; $s++) {
    echo $piloto2."<br>";
    for ($q=0; $q < 2 ; $q++) { 
        $piloto3.=$piloto4;
        echo$piloto3;
        echo"<br>";
    }
    for ($f=0; $f < 1; $f++) { 
        $piloto4.=$piloto2;
        echo $piloto4."<br>";
        echo $piloto2;

    }
}

echo"<br>";
echo"<br>";

// Ejercicio 5


$espacios = "&nbsp";
$unpiloto = "*";

for ($z=0; $z < 9; $z = $z + 2) { 
    for ($c=0; $c < 9 - $z - 1; $c++) { 
       echo $espacios;
    }
    for ($c=0; $c <= $z; $c++) { 
        echo $unpiloto;
    }
    echo"<br>";
    
}


?>
</body>
</html>